package com.utsav.news;

import com.utsav.news.data.model.NewsArticle;

import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Unit tests for the EmailValidator logic.
 */
public class NewsArticleTest {
    @Test
    public void newsArticleValidator_INValidDomainURL_ReturnsFalse() {
        assertFalse(NewsArticle.isValidUrl("abc1234567"));
    }

    @Test
    public void newsArticleValidator_ValidDomainURL_ReturnsTrue() {
        assertTrue(NewsArticle.isValidUrl("google.com"));
    }



}
