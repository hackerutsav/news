package com.utsav.news.di.app;

import com.utsav.news.ui.topnews.fragment.HeadlinesFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector(modules = ViewModelModule.class)
    abstract HeadlinesFragment contributesHeadlinesFragment();
}
