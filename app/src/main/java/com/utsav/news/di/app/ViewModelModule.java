package com.utsav.news.di.app;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;


import com.utsav.news.ui.topnews.viewmodel.HeadlinesViewModel;
import com.utsav.news.viewmodel.NewsViewModelFactory;
import com.utsav.news.viewmodel.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HeadlinesViewModel.class)
    abstract ViewModel bindHeadlinesViewModel(HeadlinesViewModel headlinesViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(NewsViewModelFactory factory);
}
