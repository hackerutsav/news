package com.utsav.news.ui.topnews.adapter;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.utsav.news.R;
import com.utsav.news.data.model.NewsArticle;
import com.utsav.news.databinding.NewsArticleItemBinding;
import com.utsav.news.ui.common.DataBoundListAdapter;

public class NewsListAdapter extends DataBoundListAdapter<NewsArticle, NewsArticleItemBinding>
{
    private final DataBindingComponent dataBindingComponent;
    private final NewsArticleClickCallback newsArticleClickCallback;

    public NewsListAdapter(DataBindingComponent dataBindingComponent, NewsArticleClickCallback newsArticleClickCallback) {
        this.dataBindingComponent = dataBindingComponent;
        this.newsArticleClickCallback = newsArticleClickCallback;
    }

    @Override
    protected NewsArticleItemBinding createBinding(ViewGroup parent) {
        NewsArticleItemBinding binding = DataBindingUtil.
                inflate(LayoutInflater.from(parent.getContext()),  R.layout.news_article_item,
                        parent, false, dataBindingComponent);

        return binding;
    }

    @Override
    protected void bind(NewsArticleItemBinding binding, NewsArticle item) {
        binding.newsImage.setImageDrawable(null);
        binding.setNewsArticle(item);
        binding.newsCard.setOnClickListener(view -> newsArticleClickCallback.onClick(item));

    }

    @Override
    protected boolean areItemsTheSame(NewsArticle oldItem, NewsArticle newItem) {
        return false;
    }

    @Override
    protected boolean areContentsTheSame(NewsArticle oldItem, NewsArticle newItem) {
        return false;
    }

    public interface NewsArticleClickCallback {
        void onClick(NewsArticle newsArticle);
    }
}
