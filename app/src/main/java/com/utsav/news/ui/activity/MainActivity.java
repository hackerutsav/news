package com.utsav.news.ui.activity;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.LruCache;

import com.jakewharton.disklrucache.DiskLruCache;
import com.utsav.news.R;
import com.utsav.news.base.activity.BaseActivity;
import com.utsav.news.ui.controller.NavigationController;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


import javax.inject.Inject;


import static android.os.Environment.isExternalStorageRemovable;

public class MainActivity extends BaseActivity {
    private LruCache<String, Bitmap> memoryCache;

    private DiskLruCache diskLruCache;
    private final Object diskCacheLock = new Object();
    private boolean diskCacheStarting = true;
    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
    private static final String DISK_CACHE_SUBDIR = "thumbnails";


    @Inject
    NavigationController navigationController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle(getString(R.string.app_title));

        if (savedInstanceState == null) {
            navigationController.showHeadlines();
        }



        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory/8;

        memoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };


        // Initialize disk cache on background thread
        File cacheDir = getDiskCacheDir(this, DISK_CACHE_SUBDIR);
        new InitDiskCacheTask().execute(cacheDir);






    }

    public void addBitmapToCache(String key, Bitmap bitmap) throws IOException {
        try {
            // Add to memory cache as before
            if (getBitmapFromMemCache(key) == null) {
                memoryCache.put(key, bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Also add to disk cache
        synchronized (diskCacheLock) {
            if (diskLruCache != null && diskLruCache.get(key) == null) {
                DiskLruCache.Editor editor = null;

                try {



                        editor = diskLruCache.edit(key);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 95, editor.newOutputStream(0));
                        editor.commit();


                } catch (IOException e) {
                    e.printStackTrace();

                    if (editor != null) {
                        try {
                            editor.abort();
                        } catch (IOException ignored) {

                        }
                    }
                }
            }
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }


    // Creates a unique subdirectory of the designated app cache directory. Tries to use external
// but if not mounted, falls back on internal storage.
    public File getDiskCacheDir(Context context, String uniqueName) {
        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        final String cachePath =
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                        !isExternalStorageRemovable() ? getExternalCacheDir().getPath() :
                        context.getCacheDir().getPath();

        return new File(cachePath + File.separator + uniqueName);
    }

    class InitDiskCacheTask extends AsyncTask<File, Void, Void> {
        @Override
        protected Void doInBackground(File... params) {
            synchronized (diskCacheLock) {
                File cacheDir = params[0];
                try {
                    diskLruCache = DiskLruCache.open(cacheDir,1,1, DISK_CACHE_SIZE);
                    diskCacheStarting = false; // Finished initialization
                    diskCacheLock.notifyAll(); // Wake any waiting threads
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }
    }

    public Bitmap getBitmapFromDiskCache(String key) throws IOException {
        synchronized (diskCacheLock) {
            Bitmap bm = null;
            if(diskLruCache != null){
                //diskCache.flush()
                String cacheKey = key;
                try {
                    DiskLruCache.Snapshot snapshot = diskLruCache.get(cacheKey);
                    if(snapshot != null){
                        InputStream in = snapshot.getInputStream(0);
                        bm = BitmapFactory.decodeStream(in);
                        in.close();
                        snapshot.close();
                        snapshot = null;
                        //Log.d(Constants.TAG_EMOP, "read from disk key:" + cacheKey + ", url:" + url.toString());
                    }
                } catch (IOException e) {
                    Log.d("error", "read disk lru cache error:" + e.toString(), e);
                }
            }else {
                Log.w("error", "read disk lru cache is null");
            }

            return bm;
        }

    }


}
