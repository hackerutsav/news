package com.utsav.news.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;


import com.utsav.news.R;

import java.net.InetAddress;

public class NewsArticleActivity extends AppCompatActivity {
    
    WebView webView;
    private ProgressDialog dialog;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isNetworkAvailable()) {
            setContentView(R.layout.activity_news_article);
            webView = findViewById(R.id.web_view);


            webView.getSettings().setJavaScriptEnabled(true);

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.loadUrl(getIntent().getStringExtra("url"));
        }
        else
        {
            Toast.makeText(this,getResources().getString(R.string.no_internet),Toast.LENGTH_LONG).show();
        }

    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
