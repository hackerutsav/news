package com.utsav.news.ui.controller;

import android.support.v4.app.FragmentManager;

import com.utsav.news.R;
import com.utsav.news.ui.activity.MainActivity;
import com.utsav.news.ui.topnews.fragment.HeadlinesFragment;

import javax.inject.Inject;

public class NavigationController {

    private final int containerId;
    private final FragmentManager fragmentManager;

    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void showHeadlines() {
        HeadlinesFragment headlinesFragment = new HeadlinesFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, headlinesFragment)
                .commitAllowingStateLoss();
    }
}
