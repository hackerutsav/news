package com.utsav.news.ui.topnews.fragment;


import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.utsav.news.R;
import com.utsav.news.binding.FragmentDataBindingComponent;
import com.utsav.news.data.model.NewsArticle;
import com.utsav.news.data.network.Resource;
import com.utsav.news.data.network.Status;
import com.utsav.news.databinding.HeadlinesFragmentBinding;
import com.utsav.news.ui.activity.MainActivity;
import com.utsav.news.ui.activity.NewsArticleActivity;
import com.utsav.news.ui.topnews.adapter.NewsListAdapter;
import com.utsav.news.util.AutoClearedValue;
import com.utsav.news.ui.topnews.viewmodel.HeadlinesViewModel;
import com.utsav.news.viewmodel.NewsViewModelFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class HeadlinesFragment extends DaggerFragment implements NewsListAdapter.NewsArticleClickCallback {

    @Inject
    NewsViewModelFactory viewModelFactory;

    HeadlinesViewModel headlinesViewModel;

    DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    AutoClearedValue<HeadlinesFragmentBinding> binding;
    AutoClearedValue<NewsListAdapter> adapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (isNetworkAvailable()) {

            headlinesViewModel = ViewModelProviders.of(this, viewModelFactory).get(HeadlinesViewModel.class);

            LiveData<Resource<List<NewsArticle>>> resourceLiveData = headlinesViewModel.getNewsArticles();

//        resourceLiveData.observe(this, newsResource -> {
//            //data = newsResource.data;
//            try {
//                if (newsResource != null && newsResource.data != null ) {
//                    List<NewsArticle> articles = newsResource.data;
//
//                    for (NewsArticle article : articles) {
//                        Timber.d(article.getDescription());
//                    }
//                }
//            } catch (Exception e) {
//                Timber.e(e);
//            }
//        });

            NewsListAdapter newsListAdapter = new NewsListAdapter(dataBindingComponent, (NewsListAdapter.NewsArticleClickCallback) this);
            this.adapter = new AutoClearedValue<>(this, newsListAdapter);
            binding.get().newsList.setAdapter(newsListAdapter);

            resourceLiveData.observe(this, newsResource -> {

                if (newsResource != null && newsResource.data != null && newsResource.status == Status.SUCCESS) {
                    adapter.get().replace(newsResource.data);
                } else {
                    //noinspection ConstantConditions
                    adapter.get().replace(Collections.emptyList());
                }
            });
        }
        else
        {
            Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.no_internet),Toast.LENGTH_LONG).show();
        }



    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        HeadlinesFragmentBinding dataBinding = DataBindingUtil.inflate(inflater,
                R.layout.headlines_fragment, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);



        return dataBinding.getRoot();
    }


    public static HeadlinesFragment newInstance() {
        Bundle args = new Bundle();
        HeadlinesFragment fragment = new HeadlinesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onClick(NewsArticle newsArticle) {
        Intent intent = new Intent(getActivity(), NewsArticleActivity.class);
        intent.putExtra("url",newsArticle.getUrl());
        startActivity(intent);

    }

    public void addBitmapToCache(String key, Bitmap bitmap)
    {
        try {
            ((MainActivity)getActivity()).addBitmapToCache(key,bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return ((MainActivity)getActivity()).getBitmapFromMemCache(key);
    }

    public Bitmap getBitmapFromDiskCache(String key) {
        try {
            return ((MainActivity)getActivity()).getBitmapFromDiskCache(key);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


}
