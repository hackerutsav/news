package com.utsav.news.data.network.service;

import android.arch.lifecycle.LiveData;

import com.utsav.news.BuildConfig;
import com.utsav.news.data.model.News;
import com.utsav.news.data.repository.ApiResponse;

import retrofit2.http.GET;

public interface NewsService {

    @GET("top-headlines?sources=associated-press&apiKey=" + BuildConfig.API_KEY)
    LiveData<ApiResponse<News>> getTopNews();
}


