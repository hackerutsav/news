package com.utsav.news.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.utsav.news.data.model.NewsArticle;

@Database(entities =  {NewsArticle.class}, version = 1)
public abstract class NewsDatabase extends RoomDatabase{

    public abstract NewsArticleDao newsArticleDao();
}
