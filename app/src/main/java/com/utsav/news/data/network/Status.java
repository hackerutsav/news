package com.utsav.news.data.network;

/**
 * Enum used for response status.
 */
public enum Status {
    LOADING,
    SUCCESS,
    ERROR
}