package com.utsav.news.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Denny on 3/30/2018.
 */


public class News {

    private String status;

    @SerializedName(value="totalResults")
    private int totalResults;

    @SerializedName(value="articles")
    public List<NewsArticle> newsArticleLst;
}
