package com.utsav.news.binding;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import android.widget.ImageView;

import com.utsav.news.R;
import com.utsav.news.ui.topnews.fragment.HeadlinesFragment;

import java.net.URL;


import javax.inject.Inject;

/**
 * Binding adapters that work with a fragment instance.
 */
public class FragmentBindingAdapters {
    final Fragment fragment;

    @Inject
    public FragmentBindingAdapters(Fragment fragment) {
        this.fragment = fragment;
    }

    @BindingAdapter("imageUrl")
    public void bindImage(ImageView imageView, String url) {
        Bitmap bmp;

        if (!url.equalsIgnoreCase("")) {

            //getting from memcache or diskcache
            bmp = ((HeadlinesFragment) fragment).getBitmapFromMemCache(url.replaceAll("\\P{L}+", ""));
            if (bmp==null) {

                bmp = ((HeadlinesFragment) fragment).getBitmapFromDiskCache(url.replaceAll("\\P{L}+", ""));
            }

            if (bmp != null) {
                imageView.setImageBitmap(bmp);
            } else {


                new DownloadImageTask(imageView, fragment)
                        .execute(url);
            }


        }
        else
        {
            imageView.setImageResource(R.drawable.notfound);
        }
    }



//        Glide.with(fragment).load(url).into(imageView);
    }

    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        Fragment fragment;

        public DownloadImageTask(ImageView bmImage, Fragment fragment) {
            this.bmImage = bmImage;
            this.fragment = fragment;
        }


        protected Bitmap doInBackground(String... urls) {

            try {
                URL urldisplay = new URL(urls[0]);
                Bitmap mIcon11 = null;


                // Check disk cache in background thread
//                Bitmap bitmap = ((HeadlinesFragment) fragment).getBitmapFromDiskCache(urldisplay.toString());
//                if (bitmap ==null)
//                {
                mIcon11 = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(urldisplay.openConnection().getInputStream()), 400, 400, false);
//                }
                ((HeadlinesFragment) fragment).addBitmapToCache(urls[0].replaceAll("\\P{L}+", ""), mIcon11);

                return mIcon11;
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();

                return BitmapFactory.decodeResource(fragment.getResources(),
                        R.drawable.notfound);
            }

        }

        protected void onPostExecute(Bitmap result) {



                bmImage.setImageBitmap(result);


        }
    }




